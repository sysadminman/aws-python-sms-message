#!/usr/bin/python3

import boto3
import configparser


def getCredentials():
    # the createntials file is created by running aws-python-sms-create-creds.py
    config = configparser.ConfigParser()
    config.read('./aws-python-sms-create-creds.config')

    AccessKey = config['aws-python-sms']['AccessKey']
    SecretKey = config['aws-python-sms']['SecretKey']

    return AccessKey, SecretKey


def sendSMS(AccessKey, SecretKey, number, message, sender):
    client = boto3.client(
        'sns',
        aws_access_key_id=AccessKey,
        aws_secret_access_key=SecretKey,
    )

    response = client.publish(
        PhoneNumber=number,
        Message=message,
        MessageAttributes={
            'AWS.SNS.SMS.SenderID': {
                'DataType': 'String',
                'StringValue': sender,
            }
        },
    )

    print('Request ID : ' + str(response['ResponseMetadata']['RequestId']))
    print('Status Code : ' + str(response['ResponseMetadata']['HTTPStatusCode']))


AccessKey, SecretKey = getCredentials()
sendSMS(AccessKey, SecretKey, '+447XXXXXXXXX', 'Test Message', "SysAdminMan")
