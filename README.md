# README #
  
Includes a cloudformation file for setting up an account used to send SMS messages and example python3 script  
  
Running aws-python-sms-create-creds.py creates AWS credentials using a cloudformation stack. Credentials have access to send SMS messages through SNS  
Config file created is called aws-python-sms-create-creds.config  
  
Running aws-python-sms-send-message.py will use the credentials created to send a SMS message  
Just change the number and message details  
  
### How do I get set up? ###
  
git clone git@bitbucket.org:sysadminman/aws-python-sms-message.git  
install boto3 - https://boto3.readthedocs.io/en/latest/guide/quickstart.html  
  
run ./aws-python-sms-create-creds.py    - to create credentials  
run ./aws-python-sms-send-message.py    - to send SMS using those credentials  
  
### Who do I talk to? ###
  
bb@sysadminman.net  