#!/usr/bin/python3

import sys

import boto3
from botocore.exceptions import ClientError

import configparser


def createCloudformationStack(stackName):

    # read the cloudformation file
    cloudFormation = open('./aws-python-sms-message.cloudformation.json', 'r')

    client = boto3.client('cloudformation')

    # create the cloudformation stack. We need CAPABILITY_IAM because it creates IAM details
    response = client.create_stack(
        StackName=stackName,
        TemplateBody=cloudFormation.read(),
        Capabilities=[
            'CAPABILITY_IAM'
        ],
        Tags=[
            {
                'Key': 'created_for',
                'Value': 'aws-python-sms-message'
            },
        ],
    )

    stackId = response['StackId']

    return stackId


def checkIfStackExists(stackName):

    client = boto3.client('cloudformation')

    # try to restrieve the stack based on the stack name. If it doesn't exist then create one
    try:
        response = client.describe_stacks(StackName=stackName)
        stackId = response['Stacks'][0]['StackId']
        return(stackId)
    except ClientError:
        stackId = createCloudformationStack('aws-python-sms-message')
        return(stackId)


def getSMSCredentialsFromStack(stackId):

    client = boto3.client('cloudformation')

    # retrieve details about the stack
    try:
        response = client.describe_stacks(
            StackName=stackId
        )
    except ClientError:
        print("There was an error getting the stack details")

    # we assume there's only going to be one stack returned. Get the data for that stack
    data = response['Stacks'][0]

    # parse the data and set the accesskey and secretkey
    for value in data['Outputs']:
        if value['OutputKey'] == 'SecretKey':
            SecretKey = value['OutputValue']
        elif value['OutputKey'] == 'AccessKey':
            AccessKey = value['OutputValue']
        else:
            print("We did not get credentials back from AWS")
            sys.exit()

    return AccessKey, SecretKey


def createConfigFile(AccessKey, SecretKey):

    Config = configparser.ConfigParser()
    cfgfile = open('./aws-python-sms-create-creds.config', 'w')
    Config.add_section('aws-python-sms')
    Config.set('aws-python-sms', 'AccessKey', AccessKey)
    Config.set('aws-python-sms', 'SecretKey', SecretKey)
    Config.write(cfgfile)
    cfgfile.close()


# check if a cloudformation stack exists, if not create one
stackId = checkIfStackExists('aws-python-sms-message')
AccessKey, SecretKey = getSMSCredentialsFromStack(stackId)
createConfigFile(AccessKey, SecretKey)

print('Created config file ./aws-python-sms-create-creds.config')
